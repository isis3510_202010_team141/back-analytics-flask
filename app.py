from flask import Flask, request, Response
from flask_sqlalchemy import SQLAlchemy
from flask_pymongo import PyMongo
from urllib.parse import urlencode
import urllib.request, json


app = Flask(__name__)
#Llave de google e informacion para la conexion a las bases de datos
DB_URL = 'postgres://xneszmlx:5ceKYWO5Mhcu88n0FI0Q3pCgfZUKaT8X@salt.db.elephantsql.com:5432/xneszmlx'
MONGO_URL = "mongodb+srv://dev123:dev123@cluster0.e9qc8.mongodb.net/finance_analytics_db?retryWrites=true&w=majority"
GOOGLE_K = "AIzaSyDj2fKPZzd3I3gxwBF-rtsQI3SyfFmM8Tk"


app.config['GOOGLEMAPS_KEY'] = GOOGLE_K
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config["MONGO_URI"] = MONGO_URL


app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
dbPosgre = SQLAlchemy(app)
mongo = PyMongo(app)

# Metodo para dar los nombres y valores de las transacciones
@app.route('/trans', methods=['GET'])
def getTransactions():
    transact = dbPosgre.Table('api_transaction', dbPosgre.metadata, autoload=True, autoload_with=dbPosgre.engine)

    #Realizar un query a la base de datos pidiendo las transacciones
    results = dbPosgre.session.query(transact).all()
    res = []

    #Sacar la informacion deseada y luego devolverla
    for r in results:
        val= float(r.value)
        temp = {'name': r.name, 'value': val}
        res.append(temp)

    return Response(json.dumps(res),  mimetype='application/json')


# Metodo para obtener los ATMs cercanos a la posicion geografica dada
@app.route('/locations', methods=['POST'])
def postLocationByPosition():
    # Conseguir los datos que se encuentran en el json que se envia
    coordinates = request.get_json().get('coordinates')
    latitude = coordinates.get('lat')
    longitude = coordinates.get('lng')

    # Crear la url con todos los datos para realizar el request
    query_string = urlencode(
        {'key': GOOGLE_K,
         'location': str(latitude)+','+str(longitude),
         'radius': 2000,
         'fields': 'formatted_address,name,photos',
         'type': 'atm'}, doseq=True)
    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?' + query_string

    # realizar el request a la API de Google para obtener los ATMs
    with urllib.request.urlopen(url) as urlData:
        data = json.loads(urlData.read().decode())
    data = data.get('results')

    # Sacar la informacion deseada de cada elemento en el json recibido antes de devolverla
    res = []
    for i in data:
        name = i .get('name')
        photos = i.get('photos')
        opening_hours = i.get('opening_hours')
        address = i.get('vicinity')
        temp = {'name': name ,'photos': photos, 'opening_hours': opening_hours ,'address':address}
        res.append(temp)

    return Response(json.dumps(res),  mimetype='application/json')

# Metodos para acceder a los diferentes documentos (tablas en DB relacional) que se encuentran en Mongo
# Los documentos son OS, user_themePref, function_time, user_lastLocation

@app.route('/os', methods=['GET'])
def getOS():
    dbMongo = mongo.db.OS
    usersOS = dbMongo.find()
    res = [{'user_id' : os['user_id'], 'os_version' : os['os_version'], 'isAndroid': os['isAndroid']} for os in usersOS]
    #print(res)
    return Response(json.dumps(res),  mimetype='application/json')

@app.route('/usrthemepref', methods=['GET'])
def getUserTheme():
    dbMongo = mongo.db.user_themePref
    usersTheme = dbMongo.find()
    res = [{'user_id' : theme['user_id'], 'theme' : theme['theme']} for theme in usersTheme]
    #print(res)
    return Response(json.dumps(res),  mimetype='application/json')

@app.route('/functime', methods=['GET'])
def getFunctionTime():
    dbMongo = mongo.db.function_time
    funcTimes = dbMongo.find()
    res = [{'name' : fTime['name'], 'time' : fTime['time']} for fTime in funcTimes]
    #print(res)
    return Response(json.dumps(res),  mimetype='application/json')

@app.route('/usrloc', methods=['GET'])
def getUserLastLocation():
    dbMongo = mongo.db.user_lastLocation
    usersLLoc = dbMongo.find()
    res = [{'user_id' : usrLoc['user_id'], 'lat' : usrLoc['lat'],  'lng' : usrLoc['lng']} for usrLoc in usersLLoc]
    #print(res)
    return Response(json.dumps(res),  mimetype='application/json')

# Metodos para realizar POST a los diferentes documentos (tablas en DB relacional) que se encuentran en Mongo
# Los documentos son OS, user_themePref, function_time, user_lastLocation

#Solo 1 json
@app.route('/os', methods=['POST'])
def postOS():
    # Recibir los valores del json
    rJson = request.get_json().get('OS')
    userId = rJson.get('user_id')
    osV = rJson.get('os_version')
    isAnd = rJson.get('isAndroid')

    #crear el elemento e ingresarlo a la base de datos
    dbMongo = mongo.db.OS
    newEl = {'user_id' : userId, 'os_version' : osV, 'isAndroid':isAnd}
    dbMongo.insert_one(newEl)

    #Develve un mensaje
    res = {'result' : 'Created successfully'}
    print(newEl)
    return res


# Solo 1 json
@app.route('/usrthemepref', methods=['POST'])
def postUserTheme():
    # Recibir los valores del json
    rJson = request.get_json().get('Theme')
    userId = rJson.get('user_id')
    theme = rJson.get('theme')

    # crear el elemento e ingresarlo a la base de datos
    dbMongo = mongo.db.user_themePref
    newEl = {'user_id': userId, 'theme': theme}
    dbMongo.insert_one(newEl)

    # Develve un mensaje
    res = {'result': 'Created successfully'}
    print(newEl)
    return res


@app.route('/functime', methods=['POST'])
def postFunctionTime():
    # Recibir los valores del json
    rJson = request.get_json().get('FunctionTime')
    name = rJson.get('name')
    time = rJson.get('time')

    # crear el elemento e ingresarlo a la base de datos
    dbMongo = mongo.db.function_time
    newEl = {'name': name, 'time': time}
    dbMongo.insert_one(newEl)

    # Develve un mensaje
    res = {'result': 'Created successfully'}
    print(newEl)
    return res

@app.route('/usrloc', methods=['POST'])
def postUserLastLocation():
    # Recibir los valores del json
    rJson = request.get_json().get('LastLocation')
    userId = rJson.get('user_id')
    lat = rJson.get('lat')
    lng = rJson.get('lng')

    # crear el elemento e ingresarlo a la base de datos
    dbMongo = mongo.db.user_lastLocation
    newEl = {'user_id': userId, 'lat': lat, 'lng': lng}
    dbMongo.insert_one(newEl)

    # Develve un mensaje
    res = {'result': 'Created successfully'}
    print(newEl)
    return res

if __name__ == '__main__':
    app.run()
